package com.hendisantika.springbootvuejs.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-vuejs
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/10/18
 * Time: 08.17
 * To change this template use File | Settings | File Templates.
 */
@Data
@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    private String firstName;
    private String lastName;
    private String description;

    public User() {
    }

    public User(String firstName, String lastName, String desc) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = desc;
    }
}