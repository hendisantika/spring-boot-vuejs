package com.hendisantika.springbootvuejs.controller;

import com.hendisantika.springbootvuejs.entity.User;
import com.hendisantika.springbootvuejs.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-vuejs
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/10/18
 * Time: 08.24
 * To change this template use File | Settings | File Templates.
 */

@Controller
@Slf4j
public class UserController {
    @Autowired
    private UserRepository repository;

    /**
     * Display the initial screen
     *
     * @ return Go to initial screen
     */
    @GetMapping("/")
    public String index() {
        return "index";
    }

    /**
     * Set up data
     *
     * @ return Go to initial screen
     */
    @GetMapping("/setData")
    public String setData() {
        repository.save(new User("Naruto", "Uzumaki", "description 1"));
        repository.save(new User("Sasuke", "Uchiha", "Description 2"));
        repository.save(new User("Sakura", "Haruno", "description 3"));
//        log.info("Data --> {}", repository.findAll());
        return "index";
    }
}
