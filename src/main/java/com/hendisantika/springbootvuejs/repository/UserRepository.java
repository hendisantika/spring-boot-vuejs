package com.hendisantika.springbootvuejs.repository;

import com.hendisantika.springbootvuejs.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-vuejs
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/10/18
 * Time: 08.23
 * To change this template use File | Settings | File Templates.
 */
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
}
